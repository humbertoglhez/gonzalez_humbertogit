import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ControlContainer } from '@angular/forms';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

CreateFormGroup()
{
  return new FormGroup({
    Name: new FormControl('',[Validators.required,Validators.minLength(5)]),
    LastName: new FormControl(''),
    NickName: new FormControl(''),
    YearsOld: new FormControl(''),
    Gender: new FormControl(''),
    PhoneNumber: new FormControl('')
  });

}


UserControl: FormGroup;
  constructor() 
  {
    this.UserControl = this.CreateFormGroup();
   }

  ngOnInit() {
  }

  onResetForm(){
    this.UserControl.reset();
  }

  onSaveForm()
  {
    if(this.UserControl.valid)
    {
      console.log(this.UserControl.value);
      this.onResetForm();
    }
    else
    {
      console.log("No es valido");
    }

    
  }

  get name(){ return this.UserControl.get('Name');}


}


