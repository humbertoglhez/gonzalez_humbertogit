export interface IUser{

    Id: number;
    Name: string;
    LastName: string;
    NickName: string;
    YearsOld: number;
    Gender: number;
    PhoneNumber: string;

}